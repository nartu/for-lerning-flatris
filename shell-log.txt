docker run --name nginx_sb -d -p 3000:80 -v `pwd`:/usr/share/nginx/html:ro -d nginx
`pwd` = $(pwd)

docker exec -ti nginx_sb bash
conf: /etc/nginx/

docker rm -f nginx_sb

docker-compose up
docker-compose down
